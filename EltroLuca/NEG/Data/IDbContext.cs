﻿using DAO;
using System;
using System.Collections.Generic;
using System.Text;

namespace NEG.Data
{
    public interface IDbContext
    {
        public CategoriaDAO DbCategoria { get; set; }
        public void CreateCategoria();
        public void DestroyCategoria();
    }
}
