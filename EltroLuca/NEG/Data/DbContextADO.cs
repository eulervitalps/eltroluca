﻿using System;
using System.Collections.Generic;
using System.Text;
using DAO;

namespace NEG.Data
{
    public class DbContextADO : IDbContext
    {
        public readonly string _Conexao;

        public DbContextADO(string conexao)
        {
            if (string.IsNullOrEmpty(conexao))
                throw new ArgumentNullException("Conexão não pode ser fazia e nem nula.");

            _Conexao = conexao;
        }

        public CategoriaDAO DbCategoria { get; set; }

        public void CreateCategoria()
        {
            if(string.IsNullOrEmpty(_Conexao))
                throw new Exception("string de conexão inválida.");

            this.DbCategoria = new CategoriaDAO(_Conexao);
        }

        public void DestroyCategoria()
        {
            DbCategoria = null;
        }
    }
}
