﻿using EltroLuca.Shared.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EltroLuca.Client.Data
{
    public interface ICategoriaData
    {
        string Message { get; set; }
        Task<IList<Categoria>> GetAsync(int? id = null, string nome = null);
        Task<Categoria> PostAsync(Categoria model);
        Task<Categoria> GetByAsync(int id);
        Task<Categoria> PutAsync(int id, Categoria model);
        Task<object> DelAsync(int id);
    }
}
