﻿using EltroLuca.Shared.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace EltroLuca.Client.Data
{
    public class CategoriaData : ICategoriaData
    {
        private readonly HttpClient _http;

        public CategoriaData(HttpClient http)
        {
            if (http == null)
                Console.WriteLine("É no construtor mané http");

            _http = http;
        }

        public string Message { get; set; }

        public async Task<object> DelAsync(int id)
        {
            try
            {
                if (id <= 0)
                    return Task.FromException(new Exception("Erro, id deve ser mair que 0."));

                var obj = await _http.DeleteAsync($"api/categoria/{id}");

                if (obj.IsSuccessStatusCode)
                {
                    Message = "Usuário delato com sucesso.";
                    return JsonSerializer.Deserialize<object>(await obj.Content.ReadAsStringAsync());
                }

                Message = "Falha ao delatar usuário";

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return new { message = Message };
        }

        public async Task<IList<Categoria>> GetAsync(int? id = null, string nome = null)
        {
            IList<Categoria> lista = null;

            try
            {
                string url = "api/categoria";

                if (id > 0)
                    url += $"?id={id}";

                if (!string.IsNullOrEmpty(nome))
                    url += id > 0 ? $"&nome={nome}" : $"?nome={nome}";

                Console.WriteLine("Chamar API");
                Console.WriteLine($"url:{url}");

                lista = await _http.GetJsonAsync<IList<Categoria>>(url);

                Console.WriteLine("API chamada");

                return lista;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return lista ?? new List<Categoria>();
        }

        public async Task<Categoria> GetByAsync(int id)
        {
            string url = $"api/categoria/{id}";

            if (id <= 0)
                return null;

            var obj = await _http.GetJsonAsync<Categoria>(url);

            return obj;
        }

        public async Task<Categoria> PostAsync(Categoria model)
        {
            try
            {
                string url = "api/categoria";

                if (model == null)
                    return null;
                


                Console.WriteLine("Chamar API");
                Console.WriteLine($"url:{url}");

                var obj = await _http.PostJsonAsync<Categoria>(url, model);

                Console.WriteLine(obj.Id);
                return obj;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return null;
        }

        public async Task<Categoria> PutAsync(int id, Categoria model)
        {
            string url = $"api/categoria/{id}";

            if (id <= 0 || model == null)
                return null;

            var obj = await _http.PutJsonAsync<Categoria>(url, model);

            return obj;
        }
    }
}
