﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EltroLuca.Shared.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Descricao { get; set; }
        public int Qtd { get; set; }
        public Categoria Categoria { get; set; }
        public bool IsDestaque { get; set; }
        public int OrdemPrioridade { get; set; }
        public bool? Excluido { get; set; }
    }
}
