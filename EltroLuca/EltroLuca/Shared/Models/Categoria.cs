﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace EltroLuca.Shared.Models
{
    public class Categoria
    {
        public int? Id { get; set; }

        [Required(ErrorMessage = "Campo Nome é obrigatório.")]
        [StringLength(50, ErrorMessage = "O campo Nome só pode conter 50 caracteres.")]
        public string Nome { get; set; }

        [StringLength(200, ErrorMessage = "O campo Nome só pode conter 50 caracteres.")]
        public string Descricao { get; set; }

        public bool? Excluido { get; set; }
    }
}
