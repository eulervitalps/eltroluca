﻿using EltroLuca.Shared.Models;
using Microsoft.AspNetCore.Mvc;
using NEG.Data;
using System;
using System.Linq;

namespace EltroLuca.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoriaController : ControllerBase
    {
        private readonly IDbContext _dbContext;
        public CategoriaController(IDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        // GET: api/Categoria
        [HttpGet]
        public IActionResult Get(int? id, string nome)
        {
            try
            {
                _dbContext.CreateCategoria();

                Categoria categoria = new Categoria() { Id = id, Nome = nome };

                var lista = _dbContext.DbCategoria.Get(categoria);

                _dbContext.DestroyCategoria();

                return Ok(lista);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }

        // GET: api/Categoria/5
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                _dbContext.CreateCategoria();

                Categoria categoria = new Categoria() { Id = id };

                var obj = _dbContext.DbCategoria.Get(categoria).FirstOrDefault();

                _dbContext.DestroyCategoria();

                if (obj == null)
                    return NotFound(new { message = "Categoria não encontrada." });

                return Ok(obj);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }

        // POST: api/Categoria
        [HttpPost]
        public IActionResult Post([FromBody] Categoria model)
        {
            try
            {
                _dbContext.CreateCategoria();

                if (ModelState.IsValid)
                {
                    model.Id = _dbContext.DbCategoria.Set(model);

                    _dbContext.DestroyCategoria();

                    if (model.Id > 0)
                        return Ok(model);

                    return StatusCode(500, new { message = "Erro ao incluir a categoria." });
                }

                return BadRequest(ModelState.Values);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }

        // PUT: api/Categoria/5
        [HttpPut("{id}")]
        public IActionResult Put(int id, [FromBody] Categoria model)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(new { message = "Id deve ser maior que 0." });

                _dbContext.CreateCategoria();

                if (ModelState.IsValid)
                {
                    model.Id = id;
                    model.Id = _dbContext.DbCategoria.Set(model);

                    _dbContext.DestroyCategoria();

                    if (model.Id > 0)
                        return Ok(model);

                    return StatusCode(500, new { message = "Erro ao alterar a categoria." });
                }

                return BadRequest(ModelState.Values);
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            try
            {
                if (id <= 0)
                    return BadRequest(new { message = "Id deve ser maior que 0." });

                _dbContext.CreateCategoria();

                if (_dbContext.DbCategoria.Del(id) > 0)
                    return Ok(new { message = "Registro deletado com sucesso." });

                return StatusCode(500, new { message = "Erro ao deletar a categoria." });
            }
            catch (Exception ex)
            {
                return StatusCode(500, new { message = ex.Message });
            }
        }
    }
}
