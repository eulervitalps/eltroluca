IF EXISTS(SELECT * FROM SYS.procedures WHERE NAME = 'USP_CATEGORIA_INS')
BEGIN
	DROP PROC USP_CATEGORIA_INS
END
GO

CREATE PROC USP_CATEGORIA_INS
(
	 @Id INT = NULL
	,@Nome NVARCHAR(50)
	,@Descricao NVARCHAR(200) = NULL
)
AS
BEGIN
	
	IF @Id IS NULL
	BEGIN
		
		INSERT INTO Categoria
		(
			 Nome
			,Descricao
		)SELECT
			 @Nome
			,@Descricao

		SET @Id = @@IDENTITY
	END
	ELSE
	BEGIN

		UPDATE Categoria SET
			 Nome = @Nome
			,Descricao = @Descricao
			,DataAlteracao = GETDATE()
		WHERE
			Id = @Id
	END

	SELECT @Id

END
GO

IF EXISTS(SELECT * FROM SYS.procedures WHERE NAME = 'USP_CATEGORIA_DEL')
BEGIN
	DROP PROC USP_CATEGORIA_DEL
END
GO

CREATE PROC USP_CATEGORIA_DEL
(
	 @Id INT
)
AS
BEGIN
	
	UPDATE Categoria SET
		Excluido = 1
	WHERE
		Id = @Id

END
GO

IF EXISTS(SELECT * FROM SYS.procedures WHERE NAME = 'USP_CATEGORIA_SEL')
BEGIN
	DROP PROC USP_CATEGORIA_SEL
END
GO

CREATE PROC USP_CATEGORIA_SEL
(
	  @Id INT = NULL
	 ,@Nome NVARCHAR(50) = NULL
)
AS
BEGIN
	
	SELECT
		 Id
		,Nome
		,Descricao
		,Excluido
		,DataAlteracao
		,DataCadastro
	FROM
		Categoria
	WHERE
		Id = COALESCE(@Id, Id)
	AND
		Nome = COALESCE(@Nome, Nome)
	AND
		Excluido = 0
END
GO