﻿using DAO.Base;
using EltroLuca.Shared.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Text;

namespace DAO
{
    public class CategoriaDAO : BaseDAO<Categoria>, IRepositorioDAO<Categoria>
    {
        public CategoriaDAO(string conexao) :base(conexao)
        {

        }

        public CategoriaDAO(string conexao, DbTransaction _transaction = null) : base(conexao, _transaction)
        {
            isTransaction = true;
        }

        public int Del(int id)
        {
            try
            {
                cmd = new SqlCommand();
                param = new SqlParameter[1];

                MontarParametro(0, param, ParameterDirection.Input, "@Id", id, SqlDbType.Int);

                return ExecNonQuery("USP_CATEGORIA_DEL", cmd, param);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { CloseConnection(); }
        }

        public IList<Categoria> Get(Categoria obj)
        {
            try
            {
                cmd = new SqlCommand();
                param = new SqlParameter[2];
                ListRetorno = new List<Categoria>();

                MontarParametro(0, param, ParameterDirection.Input, "@Id", ObterValorOuDBNull<int>(obj.Id), SqlDbType.Int);
                MontarParametro(1, param, ParameterDirection.Input, "@Nome", obj.Nome, SqlDbType.Bit);

                using (dr = ExecReader("USP_CATEGORIA_SEL", cmd, param))
                {
                    if (dr != null)
                        while (dr.Read())
                            ListRetorno.Add(SetObject(dr));
                }

                return ListRetorno;

            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { CloseConnection(); }
        }

        public int Set(Categoria obj)
        {
            try
            {
                cmd = new SqlCommand();
                param = new SqlParameter[3];

                MontarParametro(0, param, ParameterDirection.Input, "@Id", ObterValorOuDBNull<int>(obj.Id), SqlDbType.Int);
                MontarParametro(1, param, ParameterDirection.Input, "@Nome", obj.Nome, SqlDbType.NVarChar);
                MontarParametro(2, param, ParameterDirection.Input, "@Descricao", obj.Descricao, SqlDbType.NVarChar);

                return ExecScalar("USP_CATEGORIA_INS", cmd, param, isTransaction);
            }
            catch (SqlException sqlex)
            {
                throw sqlex;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally { CloseConnection(); }
        }

        public Categoria SetObject(IDataReader dr)
        {
            try
            {
                Categoria obj = new Categoria()
                {
                    Id = GetInt32("Id", dr),
                    Nome = GetString("Nome", dr),
                    Descricao = GetString("Descricao", dr),
                    Excluido = GetBooleanNullable("Excluido", dr),
                };

                return obj;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
