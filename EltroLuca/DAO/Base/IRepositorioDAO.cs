﻿using System.Collections.Generic;
using System.Data;

namespace DAO.Base
{
    public interface IRepositorioDAO<T>
    {
        IList<T> Get(T obj);

        int Set(T obj);

        T SetObject(IDataReader dr);

        int Del(int id);
    }
}
